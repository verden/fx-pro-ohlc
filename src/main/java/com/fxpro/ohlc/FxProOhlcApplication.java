package com.fxpro.ohlc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FxProOhlcApplication {

    public static void main(String[] args) {
        SpringApplication.run(FxProOhlcApplication.class, args);
    }

}
